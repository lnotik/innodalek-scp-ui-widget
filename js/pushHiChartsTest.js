$(document).ready(function(){
	
	function  pushHiChart(){

		var widgetUniqUrl='https://push.geckoboard.com/v1/send/27769-d695260b-0d6c-4fc6-a037-e0b350aeb878';
		
		var postData = {

				api_key:'54082a6d4e7fdc964b2a99bff8a3afb4',
				chart: {
					renderTo: 'container',
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},

				title: {
					text: 'Speedometer'
				},

				pane: {
					startAngle: -150,
					endAngle: 150,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
							        [0, '#FFF'],
							        [1, '#333']
							        ]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
							        [0, '#333'],
							        [1, '#FFF']
							        ]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},

				// the value axis
				yAxis: {
					min: 0,
					max: 200,

					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',

					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: 'km/h'
					},
					plotBands: [{
						from: 0,
						to: 120,
						color: '#55BF3B' // green
					}, {
						from: 120,
						to: 160,
						color: '#DDDF0D' // yellow
					}, {
						from: 160,
						to: 200,
						color: '#DF5353' // red
					}]        
				},

				series: [{
					name: 'Speed',
					data: [80],
					tooltip: {
						valueSuffix: ' km/h'
					}
				}]

		};



		$.ajax({


			type : "POST",
			url  : widgetUniqUrl,
			data : postData,
			success: function(data){



			}


		});	

	}	
	
	
	
	pushHiChart();
	
	
	
	
});