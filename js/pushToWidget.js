var answeredCalls='';
var noansweredCalls='';
var busyCalls='';
var numberOfActiveCalls='';
var asrfor15minutes='';
var getTotalCallsFor15Minutes='';
var averageCallDurationLast15Minutes;
var maximumCallDurationLast15Minutes;
var numberOfSeconds=30;



$(document).ready(function(){

	/**
	 * This function create a jsons file 
	 * with all the statistic we need for 
	 * etc: Active calls  , last15minutes  calls ... 
	 */
	function createSCPCallsStatistics(){


		var scp_statistics_url=$("#controller_url").val() + "createScpStatistics";


		$.ajax({  

			async: false,   
			type: "POST",  
			url:  scp_statistics_url,
			dataType: "json",
			success: function(data) {  


			}

		});




	}



	/**
	 * This function push an scp information to the widget the information contains :
	 * 1) total calls for last 15 minutes
	 * 2) average success ratio of the calls , this widget takes the total amount the number of succefull calls 
	 *    an create a division (%) from the number  https://marathontelecom.geckoboard.com/ is the result
	 */
	function pushTheASRandNumberOfTotalCallsLast15minutes(){


		var widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-0482ca77-eb72-43db-9ecf-19c8d7fa5a90";
		var postData={
				"api_key":"54082a6d4e7fdc964b2a99bff8a3afb4",
				"data": {
					"item" : [ { "text" : "Total ",
						"value" : getTotalCallsFor15Minutes
					},

					{ "text" : "Success",
						"value" : asrfor15minutes
					}
					]	 
				}
		};

		$.ajax({


			type : "POST",
			url  : widgetUniqUrl,
			data : $.toJSON(postData),
			success: function(data){



			}


		});	

	}


	/**
	 * This function push an scp information to the widget the information contains
	 * the number of answered calls per last 15 minutes
	 * the not answered calls per last 15 minutes
	 * and the busyCalls per last 15 minutes
	 */
	function pushCDDistrubution(){


		var widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-e6588acd-6627-4e8e-8323-130aab8ae923";
		var postData={
				"api_key":"54082a6d4e7fdc964b2a99bff8a3afb4",
				"data": {
					"item" : 
						[
						 { "value": answeredCalls,
							 "text": "Answered Calls" 
						 },
						 {  "value": noansweredCalls, 
							 "text" : "No Answer Calls"
						 },

						 {  "value": busyCalls, 
							 "text" : "Busy Calls"
						 }
						 ]
				}
		};

		$.ajax({


			type : "POST",
			url  : widgetUniqUrl,
			data : $.toJSON(postData),
			success: function(data){



			}


		});	

	}


	/**
	 * This function pushed the active  
	 */
	function pushNumberOfActiveCallsDistrubution(){


		var widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-96a9dd3c-a831-452a-a1a3-c4d9acf0a14c";
		var postData={
				"api_key":"54082a6d4e7fdc964b2a99bff8a3afb4",
				"data": {  "item" : numberOfActiveCalls ,
					"max" : { "NumberOfCalls" : "ActiveCalls", "value" : maximumCallDurationLast15Minutes },
					"min" : { "NumberOfCalls" : "ActiveCalls",  "value" : "0"}
				}


		};

		$.ajax({


			type : "POST",
			url  : widgetUniqUrl,
			data : $.toJSON(postData),
			success: function(data){



			}


		});	

	}


	function  pushHiChart(){

		var widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-42597598-dbeb-4d14-8029-fb829e79d3d5";

		var postData = {

				api_key:'54082a6d4e7fdc964b2a99bff8a3afb4',
				chart: {
					renderTo: 'container',
					type: 'gauge',
					plotBackgroundColor: null,
					plotBackgroundImage: null,
					plotBorderWidth: 0,
					plotShadow: false
				},

				title: {
					text: 'Speedometer'
				},

				pane: {
					startAngle: -150,
					endAngle: 150,
					background: [{
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
							        [0, '#FFF'],
							        [1, '#333']
							        ]
						},
						borderWidth: 0,
						outerRadius: '109%'
					}, {
						backgroundColor: {
							linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
							stops: [
							        [0, '#333'],
							        [1, '#FFF']
							        ]
						},
						borderWidth: 1,
						outerRadius: '107%'
					}, {
						// default background
					}, {
						backgroundColor: '#DDD',
						borderWidth: 0,
						outerRadius: '105%',
						innerRadius: '103%'
					}]
				},

				// the value axis
				yAxis: {
					min: 0,
					max: 200,

					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 10,
					minorTickPosition: 'inside',
					minorTickColor: '#666',

					tickPixelInterval: 30,
					tickWidth: 2,
					tickPosition: 'inside',
					tickLength: 10,
					tickColor: '#666',
					labels: {
						step: 2,
						rotation: 'auto'
					},
					title: {
						text: 'km/h'
					},
					plotBands: [{
						from: 0,
						to: 120,
						color: '#55BF3B' // green
					}, {
						from: 120,
						to: 160,
						color: '#DDDF0D' // yellow
					}, {
						from: 160,
						to: 200,
						color: '#DF5353' // red
					}]        
				},

				series: [{
					name: 'Speed',
					data: [80],
					tooltip: {
						valueSuffix: ' km/h'
					}
				}]

		};



		$.ajax({


			type : "POST",
			url  : widgetUniqUrl,
			data : $.toJSON(postData),
			success: function(data){



			}


		});	

	}


	/**
	 * This function pushed the active  
	 */
	function pushTheAverageOfCallsIn15Minuts(){


		var widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-2d64e4c1-12c7-4eda-a953-0af704e61bc5";
		var postData={
				"api_key":"54082a6d4e7fdc964b2a99bff8a3afb4",
				"data": {  "item" : averageCallDurationLast15Minutes ,
					"max" : { "Duration" : "Max duration", "value" : maximumCallDurationLast15Minutes },
					"min" : { "Duration" : "Min duration",  "value" : "0"}
				}


		};

		$.ajax({


			type : "POST",
			url  : widgetUniqUrl,
			data : $.toJSON(postData),
			success: function(data){



			}


		});	

	}



	/**
	 * This function get the number of active calls wich took place in the last 15 minutes
	 * 
	 */
	function getNumberOfActiveCalls(){

		var  active_calls_url= $("#controller_url").val() + "numberOfActiveCalls";

		$.ajax({  

			async: false,   
			type: "POST",  
			url: active_calls_url,
			dataType: "json",
			success: function(data) {  

				numberOfActiveCalls=data['numOfActiveCalls'];

			}

		});


	}



	/**
	 * This function get the number of no answer calls wich took place in the last 15 minutes
	 * in the DB  they will be under NOANSWER section
	 */ 
	function getAnsweredCallsInTheLast15Minutes(){

		var  answered_url= $("#controller_url").val() + "getAnsweredCallsInTheLast15Minutes";

		$.ajax({  

			async: false,   
			type: "POST",  
			url: answered_url,
			dataType: "json",
			success: function(data) {  

				answeredCalls=data['answeredCallsInTheLast15Minutes'];

			}

		});
	}


	/**
	 * This function get the number of no answer calls wich took place in the last 15 minutes
	 * in the DB  they will be under NOANSWER section
	 */
	function getNoansweredCallsInTheLast15Minutes(){


		var  noanswer_url =$("#controller_url").val() + "getNoansweredCallsInTheLast15Minutes";


		$.ajax({  

			async: false,   
			type: "POST",  
			url: noanswer_url,
			dataType: "json",
			success: function(data) {  
				noansweredCalls=data['noanswerCallsInTheLast15Minutes'];

			}

		});


	}


	/**
	 * This function get the number of busy calls wich took place in the last 15 minutes
	 * in the DB  they will be under BUSY section
	 */
	function getBusyCallsInTheLast15Minutes(){


		var  busy_url=$("#controller_url").val() + "getBusyCallsInTheLast15Minutes";


		$.ajax({  

			async: false,   
			type: "POST",  
			url: busy_url,
			dataType: "json",
			success: function(data) {  

				busyCalls=data['busyCallsInTheLast15Minutes'];
			}

		});

	}




	/**
	 * This function get the average success ratio of the calls which took place 
	 * in the last 15 minutes this calls are BUSY and ANSWER only 
	 */
	function getTheASRFor15minutes(){

		var  asr_url=$("#controller_url").val() + "getTheASRinTheLast15Minutes";


		$.ajax({  

			async: false,   
			type: "POST",  
			url: asr_url,
			dataType: "json",
			success: function(data) {  

				asrfor15minutes=data['noc'];

			}

		});



	}



	/**
	 * This function get the information about the total number happend 
	 * in the last 15 minutes the there are BUSY ANSWER NO ANSWER CONGESTION CANCEL
	 * calls 
	 */
	function getTotalNOCfor15minutes(){

		var  total_number_url=$("#controller_url").val() + "getNumberOfTotalCallLast15minutes";


		$.ajax({  

			async: false,   
			type: "POST",  
			url: total_number_url,
			dataType: "json",
			success: function(data) {  

				getTotalCallsFor15Minutes=data['totalNumberOfCallsLast15minutes'];
			}

		});


	}


	/**
	 * This function get the information about the maximum
	 * call duration in the last 15 minutes
	 */
	function getMaximumCallDurationLast15Minutes(){

		var  maximum_duration_url=$("#controller_url").val() + "getTheMCDinTheLast15Minutes";


		$.ajax({  

			async: false,   
			type: "POST",  
			url:  maximum_duration_url,
			dataType: "json",
			success: function(data) {  

				maximumCallDurationLast15Minutes=data['mcd'];
			}

		});


	}



	/**
	 * This function get the information about the average
	 * call duration in the last 15 minutes
	 */
	function getAverageCallDurationLast15Minutes(){

		var  average_duration_url=$("#controller_url").val() + "getTheACDintTheLast15Minutes";


		$.ajax({  

			async: false,   
			type: "POST",  
			url:  average_duration_url,
			dataType: "json",
			success: function(data) {  

				averageCallDurationLast15Minutes=data['acd'];
			}

		});


	}





	/**
	 * This function is the "trigger" of all the system and making repeditive work by 
	 * Creating the information reading it and then push it to the https://marathontelecom.geckoboard.com/ page 
	 * for showing it in a widget .
	 */
	function repeditiveWidgetPush(){

		//Create information
		var c1 =	setInterval(createSCPCallsStatistics,numberOfSeconds*2);	


		//Get information 
		var g1 =  setInterval(getAnsweredCallsInTheLast15Minutes,numberOfSeconds*2);	
		var g2 =  setInterval(getNoansweredCallsInTheLast15Minutes,numberOfSeconds*2);
		var g3 =  setInterval(getBusyCallsInTheLast15Minutes,numberOfSeconds*2);
		var g4 =  setInterval(getNumberOfActiveCalls,numberOfSeconds*2);
		var g5 =  setInterval(getTotalNOCfor15minutes,numberOfSeconds*2);
		var g6 =  setInterval(getTheASRFor15minutes,numberOfSeconds*2);
		var g7 =  setInterval(getMaximumCallDurationLast15Minutes,numberOfSeconds*2);
		var g8 =  setInterval(getAverageCallDurationLast15Minutes,numberOfSeconds*2);

		//Push information
		var p1  =  setInterval(pushTheASRandNumberOfTotalCallsLast15minutes,numberOfSeconds*2);
		var p2  =  setInterval(pushNumberOfActiveCallsDistrubution,numberOfSeconds*2);
		var p3 =   setInterval(pushCDDistrubution,numberOfSeconds+40);
		var p4  =  setInterval(pushTheAverageOfCallsIn15Minuts,numberOfSeconds+40);


	}


	repeditiveWidgetPush();










});