<?php

define('BASE_AWT_LOCAL', "C:\\xampp\\htdocs\\innodalek-scp-ui-widget\\libs\\aws\aws.phar");
//define('BASE_AWT_SERVER', "/home/geckoboard/public_html/libs/aws/aws.phar");


if (defined('BASE_AWT_LOCAL'))
{
	require BASE_AWT_LOCAL;
}
elseif (defined('BASE_AWT_SERVER'))
{
	require BASE_AWT_SERVER;
}




Use  Aws\Common\Enum\CannedAcl;
Use  Aws\Common\Exception;
Use  Aws\S3\S3Client;


class M_postmethods extends CI_Model
{


	private $chartBuilder_f;

	function __construct()
	{
		parent::__construct();
		$this->dbApp = $this->load->database("default", TRUE);
		$this->chartBuilder_f= $this->config->item('file_path');

	}





	/**
	 * Send a POST requst using cURL
	 * @param string $url to request
	 * @param array $post values to send
	 * @param array $options for cURL
	 * @return string
	 */
	function curl_post($url,$postJS)
	{

		$headers = array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($postJS)
		);


		$ch = curl_init($url);



		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postJS);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$response = curl_exec ($ch);

		$httpResponseCode = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL );
		curl_close ($ch);

		//	echo $response;
		//	echo $httpResponseCode;

	}




	/**
	 * This function  post a json information from one server to another
	 * as it can be seen it opens stream and push the header with the body
	 * @param unknown $url the address for posting
	 * @param unknown $postJS the json string with the information for post
	 */
	function stream_post($url,$postJS)
	{


		$result = file_get_contents($url, null, stream_context_create(array(
				'http' => array(
						'method' => 'POST',
						'header' => 'Content-Type: application/json' . "\r\n"
						. 'Content-Length: ' . strlen($postJS) . "\r\n",
						'content' => $postJS
				)
		)));
	}



	/**
	 * This function push an scp information to the widget the information contains
	 * the number of answered calls per last 15 minutes
	 * the not answered calls per last 15 minutes
	 * and the busyCalls per last 15 minutes
	 */
	function pushCDDistrubution($answeredCalls,$noanswerCalls,$busyCalls,&$c_dd_pt1){

		$widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-03d20484-9bb5-4aad-8b3c-fbfaf3e702d3";

		$postData=array("api_key"=>GECKOBOARD_API_KEY,
				"data"=>array(
						"item"=>array(
								array("value"=>$answeredCalls, "text"=>"Answered Calls"),
								array("value"=>$noanswerCalls, "text"=>"No answer Calls"),
								array("value"=>$busyCalls, "text"=>"Busy Calls")

						)));
	


		$postDataJS=json_encode($postData);
		$c_dd_pt1=$postDataJS;
		$this->stream_post($widgetUniqUrl, $postDataJS);

		

	}



	/**
	 * This function push an scp information to the widget the information contains
	 * the number of  calls failed because of congestion per last 15 minutes
	 * the other answered calls per last 15 minutes
	 * and the canceled calls per last 15 minutes
	 */
	function pushCDDistrubutionPartB($congested,$chanunavail,$cancel,&$c_dd_pt2){
		$widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-09310a43-46cc-4174-80fc-945cf0b40ad3";
		//$widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-513dbe3f-39f6-4f3a-b7d4-f086e7b058f1";
			/*$postData=array("api_key"=>GECKOBOARD_API_KEY,
				"data"=>array(
						"item"=>array(
								array("value"=>$cancel,"label"=>"Canceled Calls" ,"colour"=> "FFFF10AA" ),
								array("value"=>$congested,"label"=>"Congested Calls","colour"=> "FFAA0AAA" ),
								array("value"=>$chanunavail, "label"=>"Chanunavail Calls","colour"=>"FF0000AA")


						)));*/

		
		
		
		
		
		
		$postData=array("api_key"=>GECKOBOARD_API_KEY,
				"data"=>array(
						"item"=>array(
								array("value"=>$cancel, "text"=>"Canceled Calls"),
								array("value"=>$congested, "text"=>"Congested Calls"),
								array("value"=>$chanunavail, "text"=>"Chanunavail Calls")


						)));


		$postDataJS=json_encode($postData);
		$c_dd_pt2=$postDataJS;
		$this->stream_post($widgetUniqUrl, $postDataJS);



	}
	/**
	 * This function will push the number of active calls to the
	 * geckoboard widget in the  "https://push.geckoboard.com"  the default board
	 */
	function pushNumberOfActiveCallsDistrubution($numberOfActiveCalls){


		$widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-96a9dd3c-a831-452a-a1a3-c4d9acf0a14c";
		$postDataJS=array("api_key"=>GECKOBOARD_API_KEY,
				"data"=> array("item" => $numberOfActiveCalls ,
						"max" =>array( "NumberOfCalls" => "ActiveCalls", "value" => $numberOfActiveCalls),
						"min" =>array( "NumberOfCalls" => "ActiveCalls", "value" => "0")

				));



		$postDataJS=json_encode($postDataJS);

		$this->stream_post($widgetUniqUrl, $postDataJS);



	}


	/**
	 * This function will push  the average duration of  a    call in the last 15 minutes VS the maximum call the whole day to
	 * a widget in the  "https://push.geckoboard.com" the  default board
	 */
	function pushTheAverageOfCallsIn15MinutsLegA($averageCallDurationLast15Minutes_legA,
		$maximumCallDurationLast15Minutes_legA,
	    $minimumCallDurationLast15Minutes_legA,
		&$acd_a	
			
			){

		$widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-de6adfef-b5c3-4c11-89c9-6bc307f32a0d";
	//	$widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-de6adfef-b5c3-4c11-89c9-6bc307f32a0d";
		$postDataJS=array("api_key"=>GECKOBOARD_API_KEY,

				"data"=> array("item" => $averageCallDurationLast15Minutes_legA ,
						"max" =>array( "text" => "Max length", "value" => $maximumCallDurationLast15Minutes_legA),
						"min" =>array( "text" => "Min length", "value" => $minimumCallDurationLast15Minutes_legA)

				));




		$postDataJS=json_encode($postDataJS);
        $acd_a=$postDataJS;
		$this->stream_post($widgetUniqUrl, $postDataJS);//	$this->curl_post($widgetUniqUrl, $postDataJS);


	}


	/**
	 * This function will push  the average duration of  a    call in the last 15 minutes VS the maximum call the whole day to
	 * a widget in the  "https://push.geckoboard.com" the  default board
	 */
	function pushTheAverageOfCallsIn15MinutsLegB($averageCallDurationLast15Minutes_legB,
			$maximumCallDurationLast15Minutes_legB,
			$minimumCallDurationLast15Minutes_legB,
			&$acd_b
			){
		

		$widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-d29492c4-1a11-4488-83b5-8b6b7066e03f";
		$postDataJS=array("api_key"=>GECKOBOARD_API_KEY,
				"data"=> array("item" => $averageCallDurationLast15Minutes_legB ,
						"max" =>array( "text" => "Max length" , "value" => $maximumCallDurationLast15Minutes_legB),
						"min" =>array( "text" => "Min length" ,  "value" => $minimumCallDurationLast15Minutes_legB)

				));


		$postDataJS=json_encode($postDataJS);
		$acd_b=$postDataJS;
		$this->stream_post($widgetUniqUrl, $postDataJS);



	}


	/**
	 * This function push the average success ratio
	 * @param unknown $getTotalCallsFor15Minutes
	 * @param unknown $asrfor15minutes
	 */
	function pushTheASRandNumberOfTotalCallsLast15minutes($getTotalCallsFor15Minutes,$asrfor15minuteslegA,$asrfor15minuteslegB,&$asr){


		//$widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-0b0eb68b-8e1c-45f4-b375-a253cbe1341a";
		$widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-698176d4-3360-4788-89ab-9b59cb2d1489";

		$postData=array("api_key"=>GECKOBOARD_API_KEY,
				"data"=>array(
						"item"=>array(
								array("value"=>$getTotalCallsFor15Minutes, "text"=>"Total calls"),
								array("value"=>$asrfor15minuteslegA, "text"=>"Success ratio leg A"),
								array("value"=>$asrfor15minuteslegB, "text"=>"Success ratio leg B")
									



						)));


		$postDataJS=json_encode($postData);
		$asr=$postDataJS;
		$this->stream_post($widgetUniqUrl, $postDataJS);




	}


	function pushCDDTofunelChart($total,$answered,$busy,$noanswer,$congestion,$other,$null,$chanunavail,$cancel,&$c_dd_funel_chart)
	{

		$widgetUniqUrl="https://push.geckoboard.com/v1/send/27769-07ab8757-cbeb-4124-ab80-54cc5ffad712";

		$postData=array("api_key"=>GECKOBOARD_API_KEY,
				"data"=>array("item"=>array(

						array("value"=>$total,"label"=>"TOTAL"),
						array("value"=>$answered,"label"=>"ANSWERED"),
						array("value"=>$cancel,"label"=>"CANCEL"),
						array("value"=>$busy,"label"=>"BUSY"),
						array("value"=>$noanswer,"label"=>"NOANSWER"),
						array("value"=>$congestion,"label"=>"CONGESTION"),
						array("value"=>$other,"label"=>"OTHER"),
						array("value"=>$chanunavail,"label"=>"CHANUNAVAIL")
				)));


		$postDataJS=json_encode($postData);
		$c_dd_funel_chart=$postDataJS;
		$this->stream_post($widgetUniqUrl, $postDataJS);



	}




	/**
	 * This function push the average success ratio
	 * @param unknown $getTotalCallsFor15Minutes
	 * @param unknown $asrfor15minutes
	 */
	function creatactiveVsAllCallsChart($numberOfActiveCalls,$numberOfAllCalls,&$c_activeVSall_chart)
	{


		$chartCreator="{
				chart: {
				renderTo: 'container',
				plotBackgroundColor: 'rgba(35,37,38,0)',
				backgroundColor: 'rgba(35,37,38,100)',
				borderColor: 'rgba(35,37,38,100)',
				lineColor: 'rgba(35,37,38,100)',
				plotBorderColor: 'rgba(35,37,38,100)',
				plotBorderWidth: null,
				plotShadow: false,
				height: 170
	},
				colors: [
				'#058DC7',
				'#50B432',
				'#EF561A'
				],
				credits: {
				enabled: false
	},
				title: {
				text: null
	},
				tooltip: {
				formatter: function() {
				return '<b>'+ this.point.name +'</b> '+ this.y +' calls last 15 minutes';
	}
	},
				legend: {
				borderColor: 'rgba(35,37,38,100)',
				itemWidth: 55,
				margin: 5,
				width: 200
	},
				plotOptions: {
				pie: {
				animation: true,
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
				enabled: false
	},
				showInLegend: true,
				size: '90%'
	}
	},
				series: [{
				type: 'pie',
				name: 'New vs Returning',
				data: [
				['All', ".$numberOfAllCalls."],
				{
						name: 'Active<br>',
						y: ".$numberOfActiveCalls.",
								sliced: true,
								selected: true
	}
								]
	}]
	}";




		try {
 
			$c_activeVSall_chart=$chartCreator;

			$s3_bucket="s3.marathontelecomUS";
			$s3_upload_descriptor=$chartCreator;

			$client=S3Client::factory(array(
					'key' => 'AKIAJ67GHKKITVA4622Q',
					'secret' => '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'

			));



			$uploadArray = array(

					"Bucket" => $s3_bucket,
					"Key" => 'chartSPEC.txt',
					"ACL" => "public-read",
					"Body" => $s3_upload_descriptor
			);



			$uploadID = $client->putObject($uploadArray);
			$uploadID = $client->putObject($uploadArray);


		}

		catch (Exception $e)
		{
			log_message('debug',"A S3 CODE ERROR: ".print_r($e));

		}

	}

}
?>
