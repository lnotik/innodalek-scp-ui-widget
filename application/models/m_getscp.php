<?php
/**
 *
 * @author Leonid
 * This Class helping to fetch all the information from the mw_iaa_raw
 * and calculate  the statisitc about 'ASR'  'ACD' and more in the last 15 minutes
 * most of the results writtern in a json class
 *
 */
class M_getscp extends CI_Model
{


	private $active_calls_f;
	private $averageSuccessRatioForThePast15MinutesLegA_f;
	private $averageSuccessRatioForThePast15MinutesLegB_f;

	private $averageCallDurationThePast15MinutesLegA_f;
	private $maximumCallDurationThePast15minutesLegA_f;
	private $minimumCallDurationThePast15minutesLegA_f;

	private $averageCallDurationThePast15MinutesLegB_f;
	private $maximumCallDurationThePast15minutesLegB_f;
	private $minimumCallDurationThePast15minutesLegB_f;


	private $callDispositionDistributionForBothLegs_f;

	private $statusANSWER_f;
	private $statusNOANSWER_f;
	private $statusBUSY_f;
	private $statusCONGESTED_f;
	private $statusOTHER_f;
	private $statusCANCEL_f;
	private $statusCHANUNAVAIL_f;







	private $noc_last15minutes_f;
	private $graph_noac_f;
	private $noac_f;

	function __construct()
	{
		parent::__construct();

		$this->dbApp = $this->load->database("default", TRUE);

		/*Creating a pathes to the files*/
		$this->active_calls_f=$this->config->item('file_path').ACTIVE_f;

		$this->averageSuccessRatioForThePast15MinutesLegA_f=$this->config->item('file_path').ASR15LA_f;
		$this->averageSuccessRatioForThePast15MinutesLegB_f=$this->config->item('file_path').ASR15LB_f;

		$this->averageCallDurationThePast15MinutesLegA_f=$this->config->item('file_path').ACD15LA_f;
		$this->maximumCallDurationThePast15minutesLegA_f=$this->config->item('file_path').MCD15LA_f;
		$this->minimumCallDurationThePast15minutesLegA_f=$this->config->item('file_path').MINCD15LA_f;


		$this->averageCallDurationThePast15MinutesLegB_f=$this->config->item('file_path').ACD15LB_f;
		$this->maximumCallDurationThePast15minutesLegB_f=$this->config->item('file_path').MCD15LB_f;
		$this->minimumCallDurationThePast15minutesLegB_f=$this->config->item('file_path').MINCD15LB_f;


		$this->callDispositionDistributionForBothLegs_f=$this->config->item('file_path').CDDFBL_f;

		$this->statusANSWERS_f=$this->config->item('file_path').ANSWERS_f;
		$this->statusNOANSWER_f=$this->config->item('file_path').NOANSWER_f;
		$this->statusBUSY_f=$this->config->item('file_path').BUSY_f;
		$this->statusCONGESTED_f=$this->config->item('file_path').CONGESTED_f;
		$this->statusCANCEL_f=$this->config->item('file_path').CANCEL_f;
		$this->statusOTHER_f=$this->config->item('file_path').OTHER_f;
		$this->statusCHANUNAVAIL_f=$this->config->item('file_path').CHANUNAVAIL_f;


		$this->noc_last15minutes_f=$this->config->item('file_path').NOC_LAST15MINUTES_f;
		$this->noac_f=$this->config->item('file_path').NOAC_f;

	}


	/**
	 * This function will fetch the information about the anwered calls
	 * in the last 15 minutes . All the calls registered in the mw_iaa_raw table.
	 * '900' in the $noc  = 60 * 15  (15 minutes)
	 */
	function answeredCallsInTheLast15Minutes()
	{


		$noc= "SELECT COUNT(*) AS ANSWERED FROM mw_iaa_raw WHERE (disposition_lega='ANSWER' OR  disposition_legb='ANSWER') AND
				ts_t0 BETWEEN UNIX_TIMESTAMP()  - 900 AND UNIX_TIMESTAMP() ";



		$query = $this->dbApp->query($noc);
		foreach($query->result_array() as $row)
		{
			$num_of_calls=$row['ANSWERED'];

		}

		file_put_contents($this->statusANSWERS_f ,json_encode(array('answeredCallsInTheLast15Minutes'=>$num_of_calls)));



	}


	/**
	 * This function will fetch the information about the noanswered calls
	 * in the last 15 minutes . All the calls registered in the mw_iaa_raw table.
	 * '900' in the $noc  = 60 * 15  (15 minutes)
	 */
	function noansweredCallsInTheLast15Minutes()
	{


		$noc=	"SELECT COUNT(*) AS NOANSWER FROM mw_iaa_raw WHERE (disposition_lega='NOANSWER' OR  disposition_legb='NOANSWER') AND
				ts_t0 BETWEEN UNIX_TIMESTAMP()  - 900 AND UNIX_TIMESTAMP()";



		$query = $this->dbApp->query($noc);

		foreach($query->result_array() as $row)
		{
			$num_of_calls=$row['NOANSWER'];

		}

		file_put_contents($this->statusNOANSWER_f ,json_encode(array('noanswerCallsInTheLast15Minutes'=>$num_of_calls)));



	}


	/**
	 * This function will fetch the information about the busy calls
	 * in the last 15 minutes . All the calls registered in the mw_iaa_raw table.
	 * '900' in the $noc  = 60 * 15  (15 minutes)
	 */
	function busyCallsInTheLast15Minutes()
	{

		$noc= "SELECT COUNT(*) AS BUSY FROM mw_iaa_raw WHERE (disposition_lega='BUSY' OR  disposition_legb='BUSY') AND
				ts_t0 BETWEEN UNIX_TIMESTAMP()  - 900 AND UNIX_TIMESTAMP()" ;
			


		$query = $this->dbApp->query($noc);
		foreach($query->result_array() as $row)
		{
			$num_of_calls=$row['BUSY'];

		}

		file_put_contents($this->statusBUSY_f ,json_encode(array('busyCallsInTheLast15Minutes'=>$num_of_calls)));



	}


	/**
	 * This function will fetch the information about the  calls failed due to CONGESTION
	 * in the last 15 minutes . All the calls registered in the mw_iaa_raw table.
	 * '900' in the $noc  = 60 * 15  (15 minutes)
	 */
	function congestedCallsInTheLast15Minutes()
	{

		$noc= "SELECT COUNT(*) AS CONGESTION FROM mw_iaa_raw WHERE (disposition_lega='CONGESTION' OR  disposition_legb='CONGESTION') AND
				ts_t0 BETWEEN UNIX_TIMESTAMP()  - 900 AND UNIX_TIMESTAMP()";
			

		$query = $this->dbApp->query($noc);
		foreach($query->result_array() as $row)
		{
			$num_of_calls=$row['CONGESTION'];

		}

		file_put_contents($this->statusCONGESTED_f ,json_encode(array('congestedCallsInTheLast15Minutes'=>$num_of_calls)));



	}


	/**
	 * This function will fetch the information about the  cancelled calls
	 * in the last 15 minutes . All the calls registered in the mw_iaa_raw table.
	 * '900' in the $noc  = 60 * 15  (15 minutes)
	 */
	function canceledCallsInTheLast15Minutes()
	{

		$noc= "SELECT COUNT(*) AS CANCEL FROM mw_iaa_raw WHERE (disposition_lega='CANCEL' OR  disposition_legb='CANCEL') AND
				ts_t0 BETWEEN UNIX_TIMESTAMP()  - 900 AND UNIX_TIMESTAMP()";
			

		$query = $this->dbApp->query($noc);
		foreach($query->result_array() as $row)
		{
			$num_of_calls=$row['CANCEL'];

		}

		file_put_contents($this->statusCANCEL_f ,json_encode(array('canceledCallsInTheLast15Minutes'=>$num_of_calls)));



	}



	/**
	 * This function will fetch the information about the other calls
	 * in the last 15 minutes . All the calls registered in the mw_iaa_raw table.
	 * '900' in the $noc  = 60 * 15  (15 minutes)
	 */
	function chanunavailCallsInTheLast15Minutes()
	{

		$noc= "SELECT COUNT(*) AS CHANUNAVAIL FROM mw_iaa_raw WHERE (disposition_lega='CHANUNAVAIL' OR disposition_legb='CHANUNAVAIL')
				AND ts_t0 BETWEEN UNIX_TIMESTAMP() - 900 AND UNIX_TIMESTAMP()";
			

		$query = $this->dbApp->query($noc);
		foreach($query->result_array() as $row)
		{
			$num_of_calls=$row['CHANUNAVAIL'];

		}

		file_put_contents($this->statusCHANUNAVAIL_f ,json_encode(array('chanunavailCallsInTheLast15Minutes'=>$num_of_calls)));



	}


	/**
	 * This function deliver the number of the all active calls from the mw_iaa_raw
	 * an active call is one that has a duration that is zero (did not countered yet)
	 * and exportlock is NULL which indicatates that the end of the call has not occured yet
	 *
	 */
	function active_calls()
	{
		$noc="SELECT COUNT(*) numOfCalls FROM mw_iaa_raw WHERE duration IS NULL AND exportlock IS NULL AND callserver IS NOT NULL
				AND ts_t0 BETWEEN UNIX_TIMESTAMP()-900 AND UNIX_TIMESTAMP(); ";
		$query = $this->dbApp->query($noc);

		foreach($query->result_array() as $row)
		{
			$numOfCalls=$row['numOfCalls'];

		}


		file_put_contents($this->active_calls_f, json_encode(array('numOfActiveCalls'=>$numOfCalls)));
			
	}



	/**
	 * This function deliver the number of the all calls from the mw_iaa_raw
	 * it will then used in a widget with the active calls
	 */
	function numberOfAllCalls()
	{

		$noc="SELECT COUNT(*) numOfCalls FROM mw_iaa_raw WHERE  ts_t0 BETWEEN UNIX_TIMESTAMP()-900 AND UNIX_TIMESTAMP()";
		$query = $this->dbApp->query($noc);

		foreach($query->result_array() as $row)
		{
			$numOfCalls=$row['noc'];

		}


		file_put_contents($this->noac_f, json_encode(array('noc'=>$numOfCalls)));
			

	}



	/**
	 * This function will fetch the information about all the calls
	 * in the last 15 minutes . All the calls registered in the mw_iaa_raw table.
	 * '900' in the $noc  = 60 * 15  (15 minutes)
	 */
	function numberOfCallsInTheLast15minutes(){

		$noc="SELECT COUNT(*) AS noc  FROM mw_iaa_raw WHERE ts_t0 BETWEEN UNIX_TIMESTAMP() - 900  AND UNIX_TIMESTAMP()";

		$query = $this->dbApp->query($noc);

		foreach($query->result_array() as $row)
		{
			$num_of_calls=($row['noc']);

			$num_of_calls=$num_of_calls*2; //SEE if it is ok

			file_put_contents($this->noc_last15minutes_f,json_encode(array('totalNumberOfCallsLast15minutes'=>$num_of_calls)));

		}



	}


	/**
	 * This function will calculate and fetch the information about all
	 * average success ratio for leg_a , it sums the number of "successfull calls" the answered and the busy
	 * and the widget later calculate the percentage of the success by devide this number of call
	 * in total number of calls which calculated in function  "numberOfCallsInTheLast15minutes()".
	 * All the calls registered in the mw_iaa_raw table.
	 * The when i'm counting the average success ration a NULL value on the disposition_lega
	 * mean that the call did not finished yet , so it can be addressed as a "good" call we count it in
	 * The ts_t4-ts_t3 will give us the actuall time
	 * 900 in the querry  means 15 minutes * 60 seconds each
	 * The result is stored in a json files for later use
	 */
	function averageSuccessRatioForThePast15Minutes_lega()
	{

		$answered_c=0;
		$busy_c=0;
		$total_c=0;
		$noc_success="";
		$asr="";

		$noc="SELECT disposition_lega,SUM(ts_t4-ts_t3)/60 AS billsec,COUNT(*)
				AS calls FROM mw_iaa_raw  WHERE ts_t0 BETWEEN UNIX_TIMESTAMP()-900 AND UNIX_TIMESTAMP() GROUP BY disposition_lega";




		$query = $this->dbApp->query($noc);

		foreach($query->result_array() as $row)
		{

			$disposition_lega=$row['disposition_lega'];

			switch($disposition_lega)
			{


				case "ANSWER":
					$answered_c+=$row['calls'];
					break;
				case "BUSY":
					$busy_c+=$row['calls'];
					break;
				default:
					break;

			}

			$total_c+=$row['calls'];

		}



		$noc_success= $answered_c + $busy_c ;




		if( $total_c==null ||  $total_c==0 )
		{

			file_put_contents($this->averageSuccessRatioForThePast15MinutesLegA_f,json_encode(array('asr_legA'=>"0")));

		}
		else
		{


			$asr=($noc_success/$total_c)*100;
			$asr=round($asr, 2);

			file_put_contents($this->averageSuccessRatioForThePast15MinutesLegA_f,json_encode(array('asr_legA'=>$asr)));
		}




	}


	/**
	 * This function will calculate and fetch the information about all
	 * average success ratio for leg_a , it sums the number of "successfull calls" the answered and the busy
	 * and the widget later calculate the percentage of the success by devide this number of call
	 * in total number of calls which calculated in function  "numberOfCallsInTheLast15minutes()".
	 * All the calls registered in the mw_iaa_raw table.
	 * The when i'm counting the average success ration a NULL value on the disposition_lega
	 * mean that the call did not finished yet , so it can be addressed as a "good" call we count it in
	 * The ts_t4-ts_t3 will give us the actuall time
	 * 900 in the querry  means 15 minutes * 60 seconds each
	 * The result is stored in a json files for later use
	 */
	function averageSuccessRatioForThePast15Minutes_legb()
	{


		$answered_c=0;
		$busy_c=0;
		$total_c=0;
		$noc_success="";
		$asr="";

		$noc="SELECT disposition_legb,SUM(ts_t4-ts_t3)/60 AS billsec,COUNT(*)
				AS calls FROM mw_iaa_raw  WHERE ts_t0 BETWEEN UNIX_TIMESTAMP()-900 AND UNIX_TIMESTAMP() GROUP BY disposition_legb";


		$query = $this->dbApp->query($noc);

		foreach($query->result_array() as $row)
		{

			$disposition_legb=$row['disposition_legb'];

			switch($disposition_legb)
			{


				case "ANSWER":
					$answered_c+=$row['calls'];
					break;
				case "BUSY":
					$busy_c+=$row['calls'];
					break;
				default:
					break;

			}

			$total_c+=$row['calls'];

		}

		$noc_success= $answered_c + $busy_c ;

		if( $total_c==null ||  $total_c==0 )
		{

			file_put_contents($this->averageSuccessRatioForThePast15MinutesLegB_f,json_encode(array('asr_legB'=>"0")));

		}
		else
		{


			$asr=($noc_success/$total_c)*100;
			$asr=round($asr, 2);

			file_put_contents($this->averageSuccessRatioForThePast15MinutesLegB_f,json_encode(array('asr_legB'=>$asr)));
		}







	}


	/**
	 * This function will calculate and fetch the information about all
	 * the average call duration , it sums the answered and busy today calls(successfull calls) and devide in the number
	 * of matched calls
	 * in the last 15 minutes . All the calls registered in the mw_iaa_raw table.
	 * the result is stored in a json files for later use
	 * 900 means 60 seconds * 15 minutes
	 */
	function  averageCallDurationForTheLast15Minutes_lega()
	{
		$durationSql="SELECT disposition_lega,SUM(ts_t4-ts_t3)/60 AS billsec,COUNT(*) AS
			 calls FROM mw_iaa_raw  WHERE
			 ts_t0 BETWEEN UNIX_TIMESTAMP()-900 AND UNIX_TIMESTAMP() GROUP BY disposition_lega";

		$calls="";
		$billsec="";

		$query = $this->dbApp->query($durationSql);

		foreach($query->result_array() as $row)
		{


			$disposition_lega=$row['disposition_lega'];

			switch($disposition_lega)
			{
					

				case "ANSWER":
					$calls=$row['calls'];
					$billsec=$row['billsec'];
					break;

				default:
					break;

			}

		}




		$duration_lega=round(($billsec/$calls),2);
			

			


		if($duration_lega==null)
		{

			file_put_contents($this->averageCallDurationThePast15MinutesLegA_f,json_encode(array('acd_legA'=>"0.00")));

		}
		else
		{

			file_put_contents($this->averageCallDurationThePast15MinutesLegA_f,json_encode(array('acd_legA'=>$duration_lega)));
		}
	}


	/**
	 * This function will calculate and fetch the information about the maximum
	 * call duration in the last 15 minutes . All the calls registered in the mw_iaa_raw table.
	 * The "TIME_ZONE_FLAG " is a constant which indicate the time zone
	 * and can be changed
	 * the result is stored in a json files for later use
	 */
	function  maximumCallDurationForTheLast15Minutes_lega()
	{
		$duration_lega="";
		$durationSql="SELECT (ts_t4 - ts_t3)/60 AS mcd FROM mw_iaa_raw WHERE
				(disposition_lega='ANSWER') AND
				ts_t0 BETWEEN UNIX_TIMESTAMP() - 60*15  AND UNIX_TIMESTAMP()
				HAVING mcd IS NOT NULL
				ORDER BY mcd DESC LIMIT 1";



		$query = $this->dbApp->query($durationSql);
		foreach($query->result_array() as $row)
		{



			$duration_lega=$row['mcd'];




		}

		$duration_lega=round($duration_lega,2);


		if($duration_lega=="0")
		{
			file_put_contents($this->maximumCallDurationThePast15minutesLegA_f,json_encode(array('mcd'=>"0.00")));
		}
		else
		{

			file_put_contents($this->maximumCallDurationThePast15minutesLegA_f,json_encode(array('mcd_legA'=>$duration_lega)));
		}
	}




	/**
	 * This function will calculate and fetch the information about the minimum
	 * call duration in the last 15 minutes . All the calls registered in the mw_iaa_raw table.
	 * The "TIME_ZONE_FLAG " is a constant which indicate the time zone
	 * and can be changed
	 * the result is stored in a json files for later use
	 */
	function  minimumCallDurationForTheLast15Minutes_lega()
	{

		$duration_lega="";
		$durationSql="SELECT (ts_t4 - ts_t3)/60 AS mincd FROM mw_iaa_raw WHERE
				(disposition_lega='ANSWER') AND
				ts_t0 BETWEEN UNIX_TIMESTAMP() - 60*15  AND UNIX_TIMESTAMP()
				HAVING mincd IS NOT NULL AND mincd > 0
				ORDER BY mincd ASC LIMIT 1";





		$query = $this->dbApp->query($durationSql);

		foreach($query->result_array() as $row)
		{


			$duration_lega=$row['mincd'];





		}


		$duration_lega=round($duration_lega,2);


		if($duration_lega==null)
		{

			file_put_contents($this->minimumCallDurationThePast15minutesLegA_f,json_encode(array('mincd_legA'=>"0.00")));

		}
		else
		{

			file_put_contents($this->minimumCallDurationThePast15minutesLegA_f,json_encode(array('mincd_legA'=>$duration_lega)));
		}

	}


	/**
	 * This function will calculate and fetch the information about all
	 * the average call duration , it sums the answered and busy today calls(successfull calls) and devide in the number
	 * of matched calls
	 * in the last 15 minutes . All the calls registered in the mw_iaa_raw table.
	 * the result is stored in a json files for later use
	 * 900 means 60 seconds * 15 minutes
	 */
	function  averageCallDurationForTheLast15Minutes_legb()
	{
		$durationSql="SELECT disposition_legb,SUM(ts_t4-ts_t3)/60 AS billsec,COUNT(*) AS
			 calls FROM mw_iaa_raw  WHERE
			 ts_t0 BETWEEN UNIX_TIMESTAMP()-900 AND UNIX_TIMESTAMP() GROUP BY disposition_legb";

		$duration_legb="";

		$calls="";
		$billsec="";

		$query = $this->dbApp->query($durationSql);

		foreach($query->result_array() as $row)
		{


			$disposition_legb=$row['disposition_legb'];

			switch($disposition_legb)
			{
					

				case "ANSWER" :
					$calls=$row['calls'];
					$billsec=$row['billsec'];
					break;

				default:
					break;

			}

		}




		$duration_legb=round($billsec/$calls,2);


			

		if($duration_legb==null)
		{

			file_put_contents($this->averageCallDurationThePast15MinutesLegB_f,json_encode(array('acd_legB'=>"0.00")));

		}
		else
		{

			file_put_contents($this->averageCallDurationThePast15MinutesLegB_f,json_encode(array('acd_legB'=>$duration_legb)));
		}
	}


	/**
	 * This function will calculate and fetch the information about the maximum
	 * call duration in the last 15 minutes . All the calls registered in the mw_iaa_raw table.
	 * The "TIME_ZONE_FLAG " is a constant which indicate the time zone
	 * and can be changed
	 * the result is stored in a json files for later use
	 */
	function  maximumCallDurationForTheLast15Minutes_legb()
	{
		$duration_legb="";
		$durationSql="SELECT (ts_t4 - ts_t3)/60 AS mcd FROM mw_iaa_raw WHERE
				(disposition_legb='ANSWER') AND
				ts_t0 BETWEEN UNIX_TIMESTAMP() - 60*15  AND UNIX_TIMESTAMP()
				HAVING mcd IS NOT NULL
				ORDER BY mcd DESC LIMIT 1";



		$query = $this->dbApp->query($durationSql);
		foreach($query->result_array() as $row)
		{


			$duration_legb=$row['mcd'];





		}

		$duration_legb=round($duration_legb,2);


		if($duration_legb=="0")
		{
			file_put_contents($this->maximumCallDurationThePast15minutesLegB_f,json_encode(array('mcd'=>"0.00")));
		}
		else
		{

			file_put_contents($this->maximumCallDurationThePast15minutesLegB_f,json_encode(array('mcd_legB'=>$duration_legb)));
		}
	}




	/**
	 * This function will calculate and fetch the information about the minimum
	 * call duration in the last 15 minutes . All the calls registered in the mw_iaa_raw table.
	 * the result is stored in a json files for later use
	 */
	function  minimumCallDurationForTheLast15Minutes_legb()
	{

		$duration_legb="";
		$durationSql="SELECT (ts_t4 - ts_t3)/60 AS mincd FROM mw_iaa_raw WHERE
				(disposition_legb='ANSWER') AND
				ts_t0 BETWEEN UNIX_TIMESTAMP() - 60*15  AND UNIX_TIMESTAMP()
				HAVING mincd IS NOT NULL AND mincd > 0
				ORDER BY mincd ASC LIMIT 1";






		$query = $this->dbApp->query($durationSql);

		foreach($query->result_array() as $row)
		{


			$duration_legb=$row['mincd'];




		}


		$duration_legb=round($duration_legb,2);

		if($duration_legb=="0")
		{

			file_put_contents($this->minimumCallDurationThePast15minutesLegB_f,json_encode(array('mincd_legB'=>"0.00")));

		}
		else
		{

			file_put_contents($this->minimumCallDurationThePast15minutesLegB_f,json_encode(array('mincd_legB'=>$duration_legb)));
		}

	}


	/**
	 * This function will calculate and fetch the information about the call disposition and distribution
	 * to the both legs disposition leg_a  disposition_legb  and
	 * summerize them the calculated fields are
	 * total_noc => The total number aff calls reached both legs
	 * answered_noc = The answered calls reached both legs(the answered and the busy calls are the "successfull calls")
	 * busy_noc => The busy calls reached both legs(the answered and the busy calls are the "successfull calls")
	 * noanswer_noc =>The unanswered calls reached both legs
	 * congestion_noc => The calls failed to raech their destination because of a congestion reached both legs
	 * other_noc =>The "failed" calls  that were written by the Astrisk server in both legs(failed for unknown reason)
	 * null_noc =>The "failed" calls reached both legs
	 * chanunavail_noc =>The channel unavailible  not good calls written
	 * All the calls registered in the mw_iaa_raw table.
	 * the result is stored in a json files for later use
	 */
	function CDDForBothLegs()
	{

		//	$gridinfo=array();
		$total_noc=0;
		$answered_noc=0;
		$busy_noc=0;
		$noanswer_noc=0;
		$congestion_noc=0;
		$other_noc=0;
		$null_noc=0;
		$chanunavail_noc=0;
		$cancel_noc=0;

		$CDDSqlLeg_a="SELECT disposition_lega,SUM(ts_t4-ts_t3)/60 AS billsec,COUNT(*)
				AS calls FROM mw_iaa_raw  WHERE ts_t0 BETWEEN UNIX_TIMESTAMP()-900 AND UNIX_TIMESTAMP() GROUP BY (disposition_lega)";

		$CDDSqlLeg_b="SELECT disposition_legb,SUM(ts_t4-ts_t3)/60 AS billsec,COUNT(*)
				AS calls FROM mw_iaa_raw  WHERE ts_t0 BETWEEN UNIX_TIMESTAMP()-900 AND UNIX_TIMESTAMP() GROUP BY (disposition_legb)";

		$query = $this->dbApp->query($CDDSqlLeg_a);

		foreach($query->result_array() as $row)
		{

			$disposition_lega=$row['disposition_lega'];

			//	echo $disposition_lega;
			//	echo "<br>";

			switch($disposition_lega)
			{
					
				case "ANSWER" :
					$answered_noc+=$row['calls'];
					break;
				case "" :
					$other_noc+=$row['calls'];
					break;
				case "BUSY" :
					$busy_noc+=$row['calls'];
					break;
				case "CHANUNAVAIL" :
					$chanunavail_noc+=$row['calls'];
					break;
				case "CONGESTION" :
					$congestion_noc+=$row['calls'];
					break;
				case "NOANSWER" :
					$noanswer_noc+=$row['calls'];
					break;
				case "ANSWER" :
					$answered_noc+=$row['calls'];
					break;
				case "CANCEL" :
					$cancel_noc+=$row['calls'];
				 break;

				case "(NULL)" :
					$null_noc+=$row['calls'];
					break;

				default:
					break;

			}



			$total_noc+=$row['calls'];

		}

		$query = $this->dbApp->query($CDDSqlLeg_b);

		foreach($query->result_array() as $row)
		{

			$disposition_legb=$row['disposition_legb'];

			//echo $disposition_legb;
			//echo "<br>";

			switch($disposition_legb)
			{
					
				case "ANSWER" :
					$answered_noc+=$row['calls'];
					break;
				case "" :
					$other_noc+=$row['calls'];
					break;
				case "BUSY" :
					$busy_noc+=$row['calls'];
					break;
				case "CHANUNAVAIL" :
					$chanunavail_noc+=$row['calls'];
					break;
				case "CONGESTION" :
					$congestion_noc+=$row['calls'];
					break;
				case "NOANSWER" :
					$noanswer_noc+=$row['calls'];
					break;
				case "ANSWER" :
					$answered_noc+=$row['calls'];
					break;
				case "CANCEL" :
					$cancel_noc+=$row['calls'];
					break;
				case "(NULL)" :
					$null_noc+=$row['calls'];
					break;


				default:
					break;

			}



			$total_noc+=$row['calls'];

		}



			

		$gridinfo=array(
				'total'=> $total_noc,
				'answer'=> $answered_noc,
				'busy'=>$busy_noc,
				'noanswer'=>$noanswer_noc,
				'congestion'=>$congestion_noc,
				'other'=>$other_noc,
				'null'=>$null_noc,
				'chanunavail'=>$chanunavail_noc,
				'cancel'=>$cancel_noc
		);



			
		file_put_contents($this->callDispositionDistributionForBothLegs_f,json_encode($gridinfo));





	}

	/**
	 * This function saves the json statistics were made by
	 * @param unknown $c_dd_pt1
	 * @param unknown $c_dd_pt2
	 * @param unknown $acd_a
	 * @param unknown $acd_b
	 * @param unknown $asr
	 * @param unknown $c_dd_funel_chart
	 * @param unknown $c_activeVSall_chart
	 */
	function insertSCPStatisticsAsJsons($c_dd_pt1,
			$c_dd_pt2,
			$acd_a,
			$acd_b,
			$asr,
			$c_dd_funel_chart,
			$c_activeVSall_chart)
	{




		$statSql="INSERT  INTO scp_statistics(id,c_dd_pt1,c_dd_pt2,acd_a,acd_b,asr,c_activeVSall_chart,c_dd_funel_chart)
	 		VALUES  (UNIX_TIMESTAMP(NOW()),?,?,?,?,?,?,?)";

        $query = $this->dbApp->query($statSql,array($c_dd_pt1,$c_dd_pt2,$acd_a,$acd_b,$asr,$c_activeVSall_chart,$c_dd_funel_chart));

	    return 	($query != null ) ? true :  false ;
		


	}




}

?>