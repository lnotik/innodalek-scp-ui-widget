<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



/*

|--------------------------------------------------------------------------

| File and Directory Modes

|--------------------------------------------------------------------------

|

| These prefs are used when checking and setting modes when working

| with the file system.  The defaults are fine on servers with proper

| security, but you may wish (or even need) to change the values in

| certain environments (Apache running a separate process for each

| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should

| always be used to set the mode correctly.

|

*/

define('FILE_READ_MODE', 0644);

define('FILE_WRITE_MODE', 0666);

define('DIR_READ_MODE', 0755);

define('DIR_WRITE_MODE', 0777);



/*

|--------------------------------------------------------------------------

| File Stream Modes

|--------------------------------------------------------------------------

|

| These modes are used when working with fopen()/popen()

|

*/



define('FOPEN_READ',							'rb');

define('FOPEN_READ_WRITE',						'r+b');

define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care

define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care

define('FOPEN_WRITE_CREATE',					'ab');

define('FOPEN_READ_WRITE_CREATE',				'a+b');

define('FOPEN_WRITE_CREATE_STRICT',				'xb');

define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');








define('ACTIVE_f',	           "active_calls.json");

define('ASR15LA_f',	           "averageSuccessRatioLast15MinutesLegA.json");

define('ASR15LB_f',	           "averageSuccessRatioLast15MinutesLegB.json");


define('ACD15LA_f',	           "averageCallDurationLast15MinutesLegA.json");

define('MCD15LA_f',	           "maximumCallDurationLast15MinutesLegA.json");

define('MINCD15LA_f',	       "minimumCallDurationLast15MinutesLegA.json");


define('ACD15LB_f',	           "averageCallDurationLast15MinutesLegB.json");

define('MCD15LB_f',	           "maximumCallDurationLast15MinutesLegB.json");

define('MINCD15LB_f',	       "minimumCallDurationLast15MinutesLegB.json");



/*Widget files*/



define('ANSWERS_f',	           "answersInTheLast15Minutes.json");

define('NOANSWER_f',	       "noanswerInTheLast15Minutes.json");

define('BUSY_f',	           "busyInTheLast15Minutes.json");

define('CONGESTED_f',	       "congestedInTheLast15Minutes.json");

define('OTHER_f',              "otherInTheLast15Minutes.json");

define('CANCEL_f',             "cancelledInTheLast15Minutes.json");

define('CHANUNAVAIL_f',        "chanunavailInTheLast15Minutes.json");

define('CDDFBL_f' ,             "callDispositionDistributionForBothLegs.json");

define('NOC_LAST15MINUTES_f',  "totalNumberOfCallsLast15minutes.json");

define('NOAC_f',               "numberOfAllCalls.json");



define('GECKOBOARD_API_KEY',	"54082a6d4e7fdc964b2a99bff8a3afb4");
define('TIME_ZONE_FLAG',	    "1");
define('GREY_LOG_URL',       	"http://37.46.100.201/messages?filters[additional][keys][]=Session&filters[additional][values][]=");
define( 'URL_GRAY',          	'http://37.46.100.201:9200/graylog2/_search?q=_GWUNIQUEID:');
define('DROP_BOX_API_KEY',      'urvxljywhowbs73');
define('DROP_BOX_API_SECRET',   'b5x8c5ankadcw7h');





/* End of file constants.php */

/* Location: ./application/config/constants.php */

