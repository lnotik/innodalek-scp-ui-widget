<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This class will provide an easy way for fetching an informarmation
 * for the scp widgets in the Gecko Board site -  https://app.geckoboard.com/login
 * @author Leonid
 *
*/
class  C_scp_widgetinfo extends CI_Controller {


	private  $c_dd_pt1="";
	private  $c_dd_pt2="";
	private  $acd_a="";
	private  $acd_b="";
	private  $asr="";
	private  $c_dd_funel_chart="";
	private  $c_activeVSall_chart="";

	/**
	 * Contructor
	 */
	public function __construct(){
		parent::__construct();


		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('M_getscp','scp');
		$this->load->model('M_postmethods','postmethods');

	}

	/**
	 * main Function
	 */
	function index()
	{
		//	$this->load->view('v_pushScpInfo_head');
	}
	/******************************************************************The  call disposition distribution statistics *******************************************************************************************/
	/**
	 * This function create a call disposition distribution statistics
	 * wich reused laiter by the widgets
	 */
	function createScpStatistics(){
			
		$this->scp->answeredCallsInTheLast15Minutes();
		$this->scp->noansweredCallsInTheLast15Minutes();
		$this->scp->busyCallsInTheLast15Minutes();

		$this->scp->congestedCallsInTheLast15Minutes();
		$this->scp->canceledCallsInTheLast15Minutes();
		$this->scp->chanunavailCallsInTheLast15Minutes();

		$this->scp->active_calls();
		$this->scp->numberOfCallsInTheLast15minutes();





		$this->scp->averageSuccessRatioForThePast15Minutes_lega();
		$this->scp->averageSuccessRatioForThePast15Minutes_legb();

		$this->scp->averageCallDurationForTheLast15Minutes_lega();
		$this->scp->maximumCallDurationForTheLast15Minutes_lega();
		$this->scp->minimumCallDurationForTheLast15Minutes_lega();


		$this->scp->averageCallDurationForTheLast15Minutes_legb();
		$this->scp->maximumCallDurationForTheLast15Minutes_legb();
		$this->scp->minimumCallDurationForTheLast15Minutes_legb();


		$this->scp->CDDForBothLegs();


	}




	/**
	 * This function will  fetch the information about the call disposition and distribution
	 * to the both legs disposition leg_a  disposition_legb .
	 * The calculated fields are
	 * total_noc => The total number aff calls reached both legs
	 * answered_noc = The answered calls reached both legs(the answered and the busy calls are the "successfull calls")
	 * busy_noc => The busy calls reached both legs(the answered and the busy calls are the "successfull calls")
	 * noanswer_noc =>The unanswered calls reached both legs
	 * congestion_noc => The calls failed to raech their destination because of a congestion reached both legs
	 * other_noc =>The "failed" calls  that were written by the Astrisk server in both legs(failed for unknown reason)
	 * null_noc =>The "failed" calls reached both legs
	 * chanunavail_noc =>The channel unavailible  not good calls written
	 * All the calls registered in the mw_iaa_raw table.
	 * the result is stored in a json files for later use
	 */
	function getCDDForBothLegs()
	{
		$cdd_bothlegs="";
		$cdd_bothlegs_url=$this->config->item('file_path').CDDFBL_f;

		if(file_exists($cdd_bothlegs_url))
			$cdd_bothlegs=file_get_contents($cdd_bothlegs_url);

	 return  $cdd_bothlegs;

	}



	/**
	 * This function will fetch the number of the answered call in the last 15 minutes
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getAnsweredCallsInTheLast15Minutes()
	{

		$answered_calls="";
		$answered_calls_url=$this->config->item('file_path').ANSWERS_f;

		if(file_exists($answered_calls_url))
			$answered_calls= file_get_contents($answered_calls_url);
			
			
		return $answered_calls;

	}



	/**
	 * This function will fetch the number of the none answered calls in the last 15 minutes
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getNoansweredCallsInTheLast15Minutes()
	{

		$noanswer_calls="";
		$noanswer_calls_url=$this->config->item('file_path').NOANSWER_f;

		if(file_exists($noanswer_calls_url))
			$noanswer_calls= file_get_contents($noanswer_calls_url);


		return $noanswer_calls;

	}




	/**
	 * This function will fetch the number of the busy calls in the last 15 minutes
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getBusyCallsInTheLast15Minutes()
	{
		$busy_calls="";
		$busy_calls_url=$this->config->item('file_path').BUSY_f;

		if(file_exists($busy_calls_url))
			$busy_calls= file_get_contents($busy_calls_url);


		return $busy_calls;

	}



	/**
	 * This function will fetch the number of the congested calls in the last 15 minutes
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getCongestedCallsInTheLast15Minutes()
	{
		$congested_calls="";
		$congested_calls_url=$this->config->item('file_path').CONGESTED_f;

		if(file_exists($congested_calls_url))
			$congested_calls = file_get_contents($congested_calls_url);


		return $congested_calls;

	}



	/**
	 * This function will fetch the number of the canceled calls in the last 15 minutes
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getCanceledCallsInTheLast15Minutes()
	{
		$canceled_calls="";
		$canceled_calls_url=$this->config->item('file_path').CANCEL_f;

		if(file_exists($canceled_calls_url))
			$canceled_calls = file_get_contents($canceled_calls_url);

		return $canceled_calls;

	}


	/**
	 * This function will fetch the number of the chanunavail calls in the last 15 minutes
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getChanunavailCallsInTheLast15Minutes()
	{
		$chanunavail_calls="";
		$chanunavail_calls_url=$this->config->item('file_path').CHANUNAVAIL_f;

		if(file_exists($chanunavail_calls_url))
			$chanunavail_calls=file_get_contents($chanunavail_calls_url);


		return $chanunavail_calls;
	}



	/**
	 * This function will fetch the number of the active calls in the last 15 minutes
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getNumberOfActiveCalls()
	{
		$active_calls="";
		$active_calls_url=$this->config->item('file_path').ACTIVE_f;

		if(file_exists($active_calls_url))
			$active_calls= file_get_contents($active_calls_url);


		return $active_calls;

	}


	/**
	 * This function will fetch the total calls happend in the last 15 minutes and current calls
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getNumberOfTotalCallLast15minutes()
	{
		$total_calls="";
		$total_calls_url=$this->config->item('file_path').NOC_LAST15MINUTES_f;

		if(file_exists($total_calls_url))
			$total_calls= file_get_contents($total_calls_url);



		return $total_calls;
	}



	/**
	 * This function will fetch the average success ratio of the calls happend in the last 15 minutes
	 * ratio calls are the ANSWERED and BUSY  reached (callleg_a and calleg_b) / 2* total amount of calls
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getTheASRinTheLast15MinutesLegA()
	{
		$ASRLA_15="";
		$ASRLA_15_URL=$this->config->item('file_path').ASR15LA_f;

		if(file_exists($ASRLA_15_URL))
			$ASRLA_15= file_get_contents($ASRLA_15_URL);


		return $ASRLA_15;
	}



	/**
	 * This function will fetch the average success ratio of the calls happend in the last 15 minutes
	 * ratio calls are the ANSWERED and BUSY  reached (callleg_a and calleg_b) / 2* total amount of calls
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getTheASRinTheLast15MinutesLegB()
	{
		$ASRLB_15="";
		$ASRLB_15_URL=$this->config->item('file_path').ASR15LB_f;

		if(file_exists($ASRLB_15_URL))
			$ASRLB_15= file_get_contents($ASRLB_15_URL);


		return $ASRLB_15;
	}



	/**
	 * This function will fetch the maximum call duration in the last 15 minutes
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getTheMCDinTheLast15MinutesLegA()
	{
		$MCDLA_15=""; // Max call duration leg_a
		$MCDLA_15_URL=$this->config->item('file_path').MCD15LA_f;
		if(file_exists($MCDLA_15_URL))
			$MCDLA_15= file_get_contents($MCDLA_15_URL);


		return $MCDLA_15;
	}



	/**
	 * This function will fetch the minimum call duration in the last 15 minutes
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getTheMINCDinTheLast15MinutesLegA()
	{
		$MINCDLA_15="";
		$MINCDLA_15_URL=$this->config->item('file_path').MINCD15LA_f;

		if(file_exists($MINCDLA_15_URL))
			$MINCDLA_15= file_get_contents($MINCDLA_15_URL);


		return $MINCDLA_15;
	}


	/**
	 * This function will fetch the average call duration in the last 15 minutes
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getTheACDinTheLast15MinutesLegA()
	{
		$ACDLA_15="";
		$ACDLA_15_URL=$this->config->item('file_path').ACD15LA_f;

		if(file_exists($ACDLA_15_URL))
			$ACDLA_15= file_get_contents($ACDLA_15_URL);


		return $ACDLA_15;
	}


	/*-------------------------------------------------------------------------*/

	/**
	 * This function will fetch the maximum call duration in the last 15 minutes
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getTheMCDinTheLast15MinutesLegB()
	{
		$MCDLB_15=""; // MBx call duration leg_B
		$MCDLB_15_URL=$this->config->item('file_path').MCD15LB_f;
		if(file_exists($MCDLB_15_URL))
			$MCDLB_15= file_get_contents($MCDLB_15_URL);


		return $MCDLB_15;
	}



	/**
	 * This function will fetch the minimum call duration in the last 15 minutes
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getTheMINCDinTheLast15MinutesLegB()
	{
		$MINCDLB_15="";
		$MINCDLB_15_URL=$this->config->item('file_path').MINCD15LB_f;

		if(file_exists($MINCDLB_15_URL))
			$MINCDLB_15= file_get_contents($MINCDLB_15_URL);


		return $MINCDLB_15;
	}


	/**
	 * This function will fetch the average call duration in the last 15 minutes
	 * the calls stored in a json file in the server
	 * the information will be sent to the widgets by those functions
	 */
	function getTheACDinTheLast15MinutesLegB()
	{
		$ACDLB_15="";
		$ACDLB_15_URL=$this->config->item('file_path').ACD15LB_f;

		if(file_exists($ACDLB_15_URL))
			$ACDLB_15= file_get_contents($ACDLB_15_URL);


		return $ACDLB_15;
	}






	/**
	 * This function will fetch the number of all calls (NOAC)
	 * the calls stored in a json file
	 * the information will be sent to the widgets by those functions
	 */
	function getNumberOfAllCalls()
	{
		$NOAC="";
		$NOAC_URL=$this->config->item('file_path').NOAC_f;

		if(file_exists($NOAC_URL))
			$NOAC= file_get_contents($NOAC_URL);


		return $NOAC;
	}




	/**
	 * This function push an scp information to the widget the information contains
	 * the number of answered calls per last 15 minutes
	 * the not answered calls per last 15 minutes
	 * and the busyCalls per last 15 minutes
	 */
	function pushCDDistrubution()
	{



		$getTotalCallsFor15MinutesJS=$this->getNumberOfTotalCallLast15minutes();
		$getTotalCallsFor15MinutesJS=json_decode($getTotalCallsFor15MinutesJS,true);
		$totalCallsFor15Minutes=$getTotalCallsFor15MinutesJS['totalNumberOfCallsLast15minutes'];


			
		$CDDForBothLegs = $this->getCDDForBothLegs();
		$CDDForBothLegs = json_decode($CDDForBothLegs,true);
			
			


		$answeredCallsInTheLast15Minutes=$CDDForBothLegs['answer'];
		$noAnswerCallsInTheLast15Minutes=$CDDForBothLegs['noanswer'];
		$busyCallsInTheLast15Minutes=$CDDForBothLegs['busy'];



		/*	$answeredCallsJS=$this->getAnsweredCallsInTheLast15Minutes();
		 $answeredCallsJS=json_decode($answeredCallsJS,true);
		$answeredCalls=$answeredCallsInTheLast15MinutesJS['answeredCallsInTheLast15Minutes'];


		$noAnswerCallsJS=$this->getNoansweredCallsInTheLast15Minutes();
		$noAnswerCallsJS=json_decode($noAnswerCallsJS,true);
		$noAnswerCalls=$noAnswerCallsJS['noanswerCallsInTheLast15Minutes'];


		$busyCallsJS=$this->getBusyCallsInTheLast15Minutes();
		$busyCallsJS=json_decode($busyCallsJS,true);
		$busyCalls=$busyCallsJS['busyCallsInTheLast15Minutes'];*/



		if($totalCallsFor15Minutes !=null || $totalCallsFor15Minutes != 0)
		{

			$answeredCallsInTheLast15Minutes=($answeredCallsInTheLast15Minutes/$totalCallsFor15Minutes) * 100;
			$noAnswerCallsInTheLast15Minutes=($noAnswerCallsInTheLast15Minutes/$totalCallsFor15Minutes) * 100;
			$busyCallsInTheLast15Minutes=($busyCallsInTheLast15Minutes/$totalCallsFor15Minutes) * 100;


			$answeredCallsInTheLast15Minutes=round($answeredCallsInTheLast15Minutes,2);
			$noAnswerCallsInTheLast15Minutes=round($noAnswerCallsInTheLast15Minutes,2);
			$busyCallsInTheLast15Minutes=round($busyCallsInTheLast15Minutes,2);


			$answeredCallsInTheLast15Minutes=$answeredCallsInTheLast15Minutes;
			$noAnswerCallsInTheLast15Minutes=$noAnswerCallsInTheLast15Minutes;
			$busyCallsInTheLast15Minutes=$busyCallsInTheLast15Minutes;
		}

		else {
			$answeredCallsInTheLast15Minutes="0.0%";
			$noAnswerCallsInTheLast15Minutes="0.0%";
			$busyCallsInTheLast15Minutes="0.0%";
		}


		$this->postmethods->pushCDDistrubution($answeredCallsInTheLast15Minutes,$noAnswerCallsInTheLast15Minutes,$busyCallsInTheLast15Minutes,$this->c_dd_pt1);

		
	}



	/**
	 * This function push an scp information to the widget the information contains
	 * the number of answered calls per last 15 minutes
	 * the not answered calls per last 15 minutes
	 * and the busyCalls per last 15 minutes
	 */
	function pushCDDistrubutionPart_B()
	{

		$getTotalCallsFor15MinutesJS=$this->getNumberOfTotalCallLast15minutes();
		$getTotalCallsFor15MinutesJS=json_decode($getTotalCallsFor15MinutesJS,true);
		$totalCallsFor15Minutes=$getTotalCallsFor15MinutesJS['totalNumberOfCallsLast15minutes'];


			
		$CDDForBothLegs = $this->getCDDForBothLegs();
		$CDDForBothLegs = json_decode($CDDForBothLegs,true);
			
			


		$congestedCallsInTheLast15Minutes=$CDDForBothLegs['congestion'];
		$canceledCallsInTheLast15Minutes=$CDDForBothLegs['cancel'];
		$chanunavailCallsInTheLast15Minutes=$CDDForBothLegs['chanunavail'];








		if($totalCallsFor15Minutes !=null || $totalCallsFor15Minutes != 0)
		{

			$congestedCallsInTheLast15Minutes=($congestedCallsInTheLast15Minutes/$totalCallsFor15Minutes) * 100;
			$canceledCallsInTheLast15Minutes=($canceledCallsInTheLast15Minutes/$totalCallsFor15Minutes) * 100;
			$chanunavailCallsInTheLast15Minutes=($chanunavailCallsInTheLast15Minutes/$totalCallsFor15Minutes) * 100;


			$congestedCallsInTheLast15Minutes=round($congestedCallsInTheLast15Minutes,2);
			$canceledCallsInTheLast15Minutes=round($canceledCallsInTheLast15Minutes,2);
			$chanunavailCallsInTheLast15Minutes=round($chanunavailCallsInTheLast15Minutes,2);


			$congestedCallsInTheLast15Minutes=$congestedCallsInTheLast15Minutes;
			$canceledCallsInTheLast15Minutes=$canceledCallsInTheLast15Minutes;
			$chanunavailCallsInTheLast15Minutes=$chanunavailCallsInTheLast15Minutes;
		}

		else {
			$congestedCallsInTheLast15Minutes="0.0%";
			$canceledCallsInTheLast15Minutes="0.0%";
			$chanunavailCallsInTheLast15Minutes="0.0%";
		}





		$this->postmethods->pushCDDistrubutionPartB($congestedCallsInTheLast15Minutes,$chanunavailCallsInTheLast15Minutes,$canceledCallsInTheLast15Minutes,$this->c_dd_pt2);

	}




	/**
	 * This function read the number of active calls from a file and then write it into the
	 * gecko widget to indicate the number of active calls at the moment
	 */
	function pushNumberOfActiveCallsDistrubution()
	{


		$numberOfActivecalls=$this->getNumberOfActiveCalls();
		$numberOfActivecalls=json_decode($numberOfActivecalls,true);
		$numberOfActivecalls=$numberOfActivecalls['numOfActiveCalls'];

		$this->postmethods->pushNumberOfActiveCallsDistrubution($numberOfActivecalls);
	}


	/**
	 * AMM- AVERAGE MAXIMUM MINIMUM
	 * This function read the average , maximum and minimum duration of a call
	 * reached disposition lega , in the last 15 minutes
	 * and write it into a widget
	 */
	function pushTheAMMOfCallsIn15MinutsLegA(){

		$averageCallDurationLast15Minutes_legA=$this->getTheACDinTheLast15MinutesLegA();
		$maximumCallDurationLast15Minutes_legA=$this->getTheMCDinTheLast15MinutesLegA();
		$minimumCallDurationLast15Minutes_legA=$this->getTheMINCDinTheLast15MinutesLegA();

			
		$averageCallDurationLast15Minutes_legA=json_decode($averageCallDurationLast15Minutes_legA,true);
		$maximumCallDurationLast15Minutes_legA=json_decode($maximumCallDurationLast15Minutes_legA,true);
		$minimumCallDurationLast15Minutes_legA=json_decode($minimumCallDurationLast15Minutes_legA,true);

		$averageCallDurationLast15Minutes=$averageCallDurationLast15Minutes_legA['acd_legA'];
		$maximumCallDurationLast15Minutes=$maximumCallDurationLast15Minutes_legA['mcd_legA'];
		$minimumCallDurationLast15Minutes=$minimumCallDurationLast15Minutes_legA['mincd_legA'];





		$this->postmethods->pushTheAverageOfCallsIn15MinutsLegA($averageCallDurationLast15Minutes
				,$maximumCallDurationLast15Minutes,
				$minimumCallDurationLast15Minutes,
				$this->acd_a);
			

	}

	/**
	 * AMM- AVERAGE MAXIMUM MINIMUM
	 * This function read the average , maximum and minimum duration of a call
	 * reached disposition legb , in the last 15 minutes
	 * and write it into a widget
	 */
	function pushTheAMMOfCallsIn15MinutsLegB(){

		$averageCallDurationLast15Minutes_legB=$this->getTheACDinTheLast15MinutesLegB();
		$maximumCallDurationLast15Minutes_legB=$this->getTheMCDinTheLast15MinutesLegB();
		$minimumCallDurationLast15Minutes_legB=$this->getTheMINCDinTheLast15MinutesLegB();

			
		$averageCallDurationLast15Minutes_legB=json_decode($averageCallDurationLast15Minutes_legB,true);
		$maximumCallDurationLast15Minutes_legB=json_decode($maximumCallDurationLast15Minutes_legB,true);
		$minimumCallDurationLast15Minutes_legB=json_decode($minimumCallDurationLast15Minutes_legB,true);

		$averageCallDurationLast15Minutes=$averageCallDurationLast15Minutes_legB['acd_legB'];
		$maximumCallDurationLast15Minutes=$maximumCallDurationLast15Minutes_legB['mcd_legB'];
		$minimumCallDurationLast15Minutes=$minimumCallDurationLast15Minutes_legB['mincd_legB'];





		$this->postmethods->pushTheAverageOfCallsIn15MinutsLegB($averageCallDurationLast15Minutes,
				$maximumCallDurationLast15Minutes,
				$minimumCallDurationLast15Minutes,
				$this->acd_b
		);
			

	}




	/**
	 * This function read the average success ratio of the calls in the last 15 minutes
	 * for leg a and send it to gecko board
	 */
	function pushTheASRandNumberOfTotalCallsLast15minutes()
	{

		$asrfor15minuteslegA=$this->getTheASRinTheLast15MinutesLegA();
		$asrfor15minuteslegA=json_decode($asrfor15minuteslegA,true);
		$asrLegA=$asrfor15minuteslegA['asr_legA'];

		$asrfor15minuteslegB=$this->getTheASRinTheLast15MinutesLegB();
		$asrfor15minuteslegB=json_decode($asrfor15minuteslegB,true);
		$asrLegB=$asrfor15minuteslegB['asr_legB'];


		$getTotalCallsFor15Minutes=$this->getNumberOfTotalCallLast15minutes();
		$getTotalCallsFor15Minutes=json_decode($getTotalCallsFor15Minutes,true);
		$getTotalCallsFor15Minutes=$getTotalCallsFor15Minutes['totalNumberOfCallsLast15minutes'];





		$this->postmethods->pushTheASRandNumberOfTotalCallsLast15minutes($getTotalCallsFor15Minutes,$asrLegA,$asrLegB,$this->asr);

	}





	/**
	 * This function create the hichart which illustrates
	 * the active call against all calls (in the past 15 minutes)
	 */
	function  graphOfNOAC()
	{
		$numberOfAllCalls=$this->getNumberOfTotalCallLast15minutes();
		$numberOfActiveCallsLast15minutes=$this->getNumberOfActiveCalls();

		$numberOfAllCalls=json_decode($numberOfAllCalls,true);
		$numberOfActiveCallsLast15minutes=json_decode($numberOfActiveCallsLast15minutes,true);

		$numberOfAllCalls=$numberOfAllCalls['totalNumberOfCallsLast15minutes'];
		$numberOfActiveCalls=$numberOfActiveCallsLast15minutes['numOfActiveCalls'];

		$this->postmethods->creatactiveVsAllCallsChart($numberOfActiveCalls,$numberOfAllCalls,$this->c_activeVSall_chart);





	}



	/**
	 * This function will  fetch the information about the call disposition and distribution
	 * to the both legs disposition leg_a  disposition_legb .
	 * The calculated fields are
	 * total_noc => The total number aff calls reached both legs
	 * answered_noc = The answered calls reached both legs(the answered and the busy calls are the "successfull calls")
	 * busy_noc => The busy calls reached both legs(the answered and the busy calls are the "successfull calls")
	 * noanswer_noc =>The unanswered calls reached both legs
	 * congestion_noc => The calls failed to raech their destination because of a congestion reached both legs
	 * other_noc =>The "failed" calls  that were written by the Astrisk server in both legs(failed for unknown reason)
	 * null_noc =>The "failed" calls reached both legs
	 * chanunavail_noc =>The channel unavailible  not good calls written
	 * All the calls registered in the mw_iaa_raw table.
	 * the result is stored in a json files for later use
	 */
	function pushCDDForBothLegs()
	{
			
			
		$total="";
		$answered="";
		$busy="";
		$noanswer="";
		$congestion="";
		$other="";
		$null="";
		$chanunavail="";
			
		$CDDForBothLegs = $this->getCDDForBothLegs();
		$CDDForBothLegs = json_decode($CDDForBothLegs,true);
			
			
			
		$total=$CDDForBothLegs['total'];
		$answered=$CDDForBothLegs['answer'];
		$busy=$CDDForBothLegs['busy'];
		$noanswer=$CDDForBothLegs['noanswer'];
		$congestion=$CDDForBothLegs['congestion'];
		$other=$CDDForBothLegs['other'];
		$null=$CDDForBothLegs['null'];
		$chanunavail=$CDDForBothLegs['chanunavail'];
		$cancel= $CDDForBothLegs['cancel'];
			

			
		$this->postmethods->pushCDDTofunelChart($total,$answered,$busy,$noanswer,$congestion,$other,$null,$chanunavail,$cancel,$this->c_dd_funel_chart);
			
			
	}



	/**
	 * This function trigger all the function and
	 * then push all the information to the gecko-board server
	 * letter can be invoked with widget and crontab
	 */
	function triggerPush()
	{
		$this->pushTheAMMOfCallsIn15MinutsLegA();
		$this->pushTheAMMOfCallsIn15MinutsLegB();

		$this->pushTheASRandNumberOfTotalCallsLast15minutes();
			
		$this->pushCDDistrubution();
		$this->pushCDDistrubutionPart_B();

		$this->pushNumberOfActiveCallsDistrubution();
		$this->graphOfNOAC();

		$this->pushCDDForBothLegs();

		
		$this->scp->insertSCPStatisticsAsJsons($this->c_dd_pt1,$this->c_dd_pt2,
				$this->acd_a,$this->acd_b,$this->asr,$this->c_dd_funel_chart,$this->c_activeVSall_chart);
		
		
	}







	/******************************************************************End of The  call disposition distribution statistics *******************************************************************************************/




}